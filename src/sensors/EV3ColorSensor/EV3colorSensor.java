package sensors.EV3ColorSensor;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class EV3colorSensor {

	public static void main(String[] args) {

		EV3ColorSensor colorSensor = new EV3ColorSensor(SensorPort.S1);

		SampleProvider colerID = colorSensor.getColorIDMode();
		float[] sample = new float[colerID.sampleSize()];

		while (Button.ESCAPE.isUp()) {
			colerID.fetchSample(sample, 0);
			LCD.drawString("id: " + sample[0], 1, 1);
			Delay.msDelay(555);
		}
	}
	
}
