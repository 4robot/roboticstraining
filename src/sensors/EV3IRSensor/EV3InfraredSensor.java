package sensors.EV3IRSensor;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class EV3InfraredSensor {
	public static void main(String[] args) {
		EV3IRSensor ir = new EV3IRSensor(SensorPort.S1);
		
		SampleProvider distanceMode = ir.getDistanceMode();
		
		float [] sample = new float[distanceMode.sampleSize()];
		
		while(Button.DOWN.isUp()){
			distanceMode.fetchSample(sample, 0);
			LCD.drawString("d: " + sample[0], 0, 2);
			Delay.msDelay(500);
		}
	}
}
