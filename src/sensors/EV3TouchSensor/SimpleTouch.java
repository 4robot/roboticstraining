package sensors.EV3TouchSensor;

import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.robotics.SampleProvider;
import lejos.robotics.filter.AbstractFilter;

class SimpleTouch extends AbstractFilter {
	private float[] sample;

	public SimpleTouch(SampleProvider source) {
		super(source);
		sample = new float[sampleSize];
	}

	public boolean isPressed() {
		super.fetchSample(sample, 0);
		if (sample[0] == 0)
			return false;
		return true;
	}

	public static void main(String[] args) {
		EV3LargeRegulatedMotor left = new EV3LargeRegulatedMotor(MotorPort.A);
		EV3LargeRegulatedMotor right = new EV3LargeRegulatedMotor(MotorPort.B);
		EV3TouchSensor it = new EV3TouchSensor(SensorPort.S1);

		Button.waitForAnyPress();
		SimpleTouch st = new SimpleTouch(it.getTouchMode());

		left.setSpeed(left.getMaxSpeed());
		right.setSpeed(right.getMaxSpeed());
		while (Button.DOWN.isUp()) {
			left.backward();
			right.backward();
			System.out.println(left.getSpeed());
			if (st.isPressed()) {
				left.stop();
				right.stop();
				left.rotate(500);
			}
		}

	}
}