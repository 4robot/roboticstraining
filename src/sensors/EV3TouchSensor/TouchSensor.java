package sensors.EV3TouchSensor;

import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.robotics.SampleProvider;

public class TouchSensor {
	public static void main(String[] args) {
		EV3LargeRegulatedMotor left = new EV3LargeRegulatedMotor(MotorPort.A);
		EV3TouchSensor touchsensor = new EV3TouchSensor(SensorPort.S1);
		
		
		// Get an instance of the Touch EV3 sensor
		EV3TouchSensor touchSensor = new EV3TouchSensor(SensorPort.S1);
		
		SampleProvider touchmode = touchsensor.getTouchMode();
		float [] sample = new float[touchmode.sampleSize()];
		
		while(Button.DOWN.isUp()){
			touchmode.fetchSample(sample, 0);
			left.rotate(720);
		}
		
		}
	}

