package LCD;

import lejos.hardware.lcd.LCD;
import lejos.utility.Delay;

public class Ev3LCD {
	public static void main(String[] args) {
	
		LCD.drawInt(5, 5, 5);
		Delay.msDelay(1000);
		
		LCD.drawString("EV3 Workshop", 0, 0);
		Delay.msDelay(2000);
	}
}
