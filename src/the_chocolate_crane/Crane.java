package the_chocolate_crane;
import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class Crane {
	// MOTORS
	static EV3MediumRegulatedMotor Gripper = new EV3MediumRegulatedMotor(
			MotorPort.B);
	static EV3LargeRegulatedMotor Base = new EV3LargeRegulatedMotor(MotorPort.A);
	static EV3LargeRegulatedMotor Middle = new EV3LargeRegulatedMotor(
			MotorPort.C);

	public static void rotate(int number) {
		// open gripper
		Gripper.rotate(800);
		// go below 250
		Middle.rotate(250);
		// close gripper
		Gripper.rotate(-800);
		// go up 250
		Middle.rotate(-250);
		// to The Color Box
		Base.rotate(-number);
		// go down 160
		Middle.rotate(130);
		// re-open gripper
		Gripper.rotate(800);
		// go up 160
		Middle.rotate(-130);
		// origin vector
		Base.rotate(number);
		// close gripper
		Gripper.rotate(-800);
	}

	public static void main(String[] args) {

		// Touch sensor
		EV3TouchSensor it = new EV3TouchSensor(SensorPort.S1);
		TouchSensor SampleTouch = new TouchSensor(it.getTouchMode());

		// Color sensor
		EV3ColorSensor cs = new EV3ColorSensor(SensorPort.S4);
		int h;

		// Code
		Middle.setSpeed(100);
		Base.setSpeed(100);
		Gripper.setSpeed(Gripper.getMaxSpeed());
		// to return the ahmed to the top
		Middle.rotate(-250);
		
		while (Button.UP.isUp()) {
			h = cs.getColorID();
			System.out.println(h);
			if (h == Color.RED) {
				System.out.println();
				rotate(150);
			} else if (h == Color.BLUE || h == Color.GREEN) {
				rotate(50);
			} else if (h == Color.YELLOW || h == Color.BROWN) {
				rotate(100);
			}
			// System.out.println(h);
		}

	}

}
