package play_sound;

import java.io.File;

import lejos.hardware.Sound;

public class PlaySound {

	// The file extension must be '.wav'.
	// Also, it must be mono (not stereo) file.
	// sample rates must be from 8K to 48K.

	public static void main(String[] args) {

		File myFile = new File("tara.wav");

		System.out.println("Playing sound...");
		Sound.playSample(myFile, 100);
	}

}
