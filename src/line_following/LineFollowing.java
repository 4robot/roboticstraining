package line_following;

import lejos.hardware.Button;
import lejos.hardware.motor.UnregulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;

public class LineFollowing {

	// the value of the sensor when it scans the white area
	private static final int whiteLightValue = 70;

	// the value of the sensor when it scans the middle of the black area
	// actually our sensor reads 7 when it scans the black area, but
	// we put 26 to make the result become negative in
	private static final int blackLightValue = 26;

	// the value of the sensor
	private int actualLightValue;
	private static float C = 1F;

	private UnregulatedMotor motor_left;
	private UnregulatedMotor motor_right;

	// the power value that we will give to motor B
	private int left_motor_power;

	// the power value that will give to motor C
	private int right_motor_power;

	public static void main(String[] haha) {

		LineFollowing line = new LineFollowing();

		line.motor_left = new UnregulatedMotor(MotorPort.D);
		line.motor_right = new UnregulatedMotor(MotorPort.A);

		EV3ColorSensor colorSensor = new EV3ColorSensor(SensorPort.S3);

		// get the "Reflected light intensity" mode
		SampleProvider redMode = colorSensor.getRedMode();

		float[] sample = new float[redMode.sampleSize()];

		while (Button.DOWN.isUp()) {
			// update the value of the sensor into the sample index[0]
			redMode.fetchSample(sample, 0);

			// convert the value of the sensor to integer
			line.actualLightValue = (int) ((100 * sample[0]));

			line.calcAndSetPower(whiteLightValue, blackLightValue);
		}

	}

	public static void forward(UnregulatedMotor ah, UnregulatedMotor ah2) {
		ah.forward();
		ah2.forward();
	}

	public void calcAndSetPower(int whiteLightValue, int blackLightValue) {
		// calculate the appropriate powers
		// these equations apply when you put the sensor to the
		// right of the black line, if you want to put it to the left line
		// then you just have to swap the two equations.

		left_motor_power = (int) ((whiteLightValue - actualLightValue) * C);
		right_motor_power = (int) ((actualLightValue - blackLightValue) * C);

		// if black is more
		if (left_motor_power > right_motor_power) {
			// if it is not right angle
			if (actualLightValue > 9) {
				left_motor_power += 30;
				right_motor_power += 30 / 2;
			}
			// if white is more
		} else if (right_motor_power > left_motor_power) {
			// if it is not right angle
			if (actualLightValue < 67) {
				right_motor_power += 30;
				left_motor_power += 30 / 2;
			}
		} else {
			right_motor_power += 30;
			left_motor_power += 30;
		}

		
		motor_left.setPower(100);
		motor_right.setPower(100);
		
		System.out.printf("L: %d, R: %d\n", motor_left.getPower(), motor_right.getPower());
	}

}