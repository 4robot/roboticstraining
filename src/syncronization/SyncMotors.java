package syncronization;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;

public class SyncMotors {

	public static void main(String[] args) {
		
		EV3LargeRegulatedMotor motor1 = new EV3LargeRegulatedMotor(MotorPort.A);
		EV3LargeRegulatedMotor motor2 = new EV3LargeRegulatedMotor(MotorPort.B);
		
		RegulatedMotor[] motors = {motor2};
		
		motor1.synchronizeWith(motors);
		
		motor1.startSynchronization();
		motor1.forward();
		motor2.forward();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		motor1.endSynchronization();
	}
	
	
}
