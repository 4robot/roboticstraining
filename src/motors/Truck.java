package motors;

import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class Truck {

	public static void main(String[] args) {

		EV3MediumRegulatedMotor truck = new EV3MediumRegulatedMotor(MotorPort.A);
		EV3LargeRegulatedMotor motor1 = new EV3LargeRegulatedMotor(MotorPort.B);
		EV3LargeRegulatedMotor motor2 = new EV3LargeRegulatedMotor(MotorPort.C);

		EV3IRSensor ir = new EV3IRSensor(SensorPort.S4);

		SampleProvider distanceMode = ir.getDistanceMode();

		float[] sample = new float[distanceMode.sampleSize()];

		truck.setStallThreshold(13, 100);
		truck.setSpeed(truck.getMaxSpeed());
		System.out.println("Press any button...");

		Button.waitForAnyPress();
		
		/*distanceMode.fetchSample(sample, 0);
		while(sample[0] > 30) {
			motor1.forward();
			motor2.forward();
			distanceMode.fetchSample(sample, 0);
		}*/

		while (!truck.isStalled()) {
			truck.forward();
		}
		
		truck.close();
		truck = null;
		truck = new EV3MediumRegulatedMotor(MotorPort.A);
		
		motor1.backward();
		motor2.backward();
		Delay.msDelay(3000);
		
		while (!truck.isStalled()) {
			truck.backward();
		}
		
		

		
		
		/*while (!truck.isStalled()) {
			truck.backward();
		}
		*/
		

	}
}