package motors;


import lejos.hardware.Button;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;
import lejos.hardware.Sound;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.Color;
import lejos.robotics.ColorAdapter;
import lejos.utility.Delay;

public class FireBallsWithColor {

	public static boolean keepGoing = true;
	
	public static void main(String[] haha) {
		
		
		
		Button.ESCAPE.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(Key k) {
				keepGoing= false;
			}
			
			@Override
			public void keyPressed(Key k) {
				
				
			}
		});
		
		EV3MediumRegulatedMotor motor = new EV3MediumRegulatedMotor(MotorPort.B);
		
		EV3ColorSensor cs = new EV3ColorSensor(SensorPort.S3);
		ColorAdapter ca = new ColorAdapter(cs);
		
		while(keepGoing) {
			Color c = ca.getColor();
			
			double total = c.getRed() + c.getBlue() + c.getGreen();
			
			double blue_ratio = c.getBlue() / total;
			double red_ratio = c.getRed() / total;
			double green_ratio = c.getGreen() / total;
			
			
			if(blue_ratio > 0.33) {
				//System.out.println(c.getBlue());
				//System.out.println(red_ratio + " ratio");
				System.out.println("BLUE");
				Sound.playTone(1000, 100);
				motor.rotate((36/12)*-360, false);
				
			} else if(red_ratio > 0.33) {
				System.out.println("RED");
				Sound.playTone(1000, 100);
				motor.rotate((36/12)*-360, false);
				
			} else if(green_ratio > 0.33) {
				System.out.println("GREEN");
				Sound.playTone(1000, 100);
				motor.rotate((36/12)*-360, false);
				
			} else {
				Sound.playTone(500, 100);
			}
			Delay.msDelay(1000);
		}
		cs.close();
		motor.close();
		
	}
	
	
	
	

	
	
	
}