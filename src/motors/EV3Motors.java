package motors;

import java.util.ArrayList;

import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.RegulatedMotorListener;
import lejos.utility.Delay;

public class EV3Motors implements RegulatedMotorListener {
	public static void main(String[] args) {

		EV3Motors m = new EV3Motors();
		EV3TouchSensor t = new EV3TouchSensor(SensorPort.S1);
		ArrayList<String> modes = t.getAvailableModes();
		// System.out.println(modes);

		m.accelerate();

		/*
		 * 
		 * page. 44 example UnregulatedMotor left = new
		 * UnregulatedMotor(MotorPort.D); UnregulatedMotor right = new
		 * UnregulatedMotor(MotorPort.A); EV3Motors m = new EV3Motors();
		 * 
		 * // left.addListener(m);
		 * 
		 * Button.waitForAnyPress(); for(int i=0 ; i<3 ;i++){
		 * Sound.playTone(500, 800); Delay.msDelay(200); } Sound.playTone(1000,
		 * 800); left.setPower(100); right.setPower(100);
		 * 
		 * 
		 * 
		 * //Sound.playTone(3000, 1000); left.forward(); right.forward();
		 * 
		 * Delay.msDelay(2500); left.flt(); right.flt();
		 * 
		 * 
		 * 
		 * LCD.drawInt((int) left.getPosition(), 0, 0);
		 * 
		 * left.rotateTo(100);
		 * 
		 * right.startSynchronization(); left.forward(); right.forward();
		 * right.endSynchronization();
		 * 
		 * 
		 * 
		 * EV3LargeRegulatedMotor right = new
		 * EV3LargeRegulatedMotor(MotorPort.A); EV3LargeRegulatedMotor left =
		 * new EV3LargeRegulatedMotor(MotorPort.B);
		 * 
		 * right.forward(); left.forward();
		 * 
		 * //Delay.msDelay(1000);
		 * 
		 * left.stop(); right.stop();
		 * 
		 * Button.waitForAnyPress();
		 * 
		 * right.backward(); left.backward();
		 * 
		 * //Delay.msDelay(1000); left.stop(); right.stop();
		 * 
		 * //getSepeed LCD.drawInt(right.getSpeed(), 0, 0);
		 * LCD.drawInt(left.getSpeed(), 0, 1);
		 * 
		 * 
		 * //setSpeed left.setSpeed(720); right.setSpeed(720);
		 * 
		 * 
		 * 
		 * // rotate right.rotate(angle);
		 * 
		 * 
		 * 
		 * 
		 * left.rotate(angle); left.rotate(angle, immediateReturn);
		 * 
		 * 
		 */

	}

	public void accelerate() {
		EV3LargeRegulatedMotor left = new EV3LargeRegulatedMotor(MotorPort.A);
		EV3LargeRegulatedMotor right = new EV3LargeRegulatedMotor(MotorPort.D);

		Button.waitForAnyPress();
		Sound.playTone(300, 1000);
		Sound.playTone(400, 1000);
		Sound.playTone(500, 1000);
		Sound.playTone(700, 1000);
		Sound.playTone(1000, 1000);

		left.setSpeed(100);
		right.setSpeed(100);

		// left.setAcceleration(100);
		// right.setAcceleration(100);

		for (int i = 0; i < 10; i++) {
			left.setSpeed(left.getSpeed() + 200);
			right.setSpeed(right.getSpeed() + 200);
			LCD.drawInt(left.getSpeed(), 0, 0);
			left.forward();
			right.forward();
			Delay.msDelay(300);
		}

		Delay.msDelay(10000);
	}

	public void rotate() {
		final EV3LargeRegulatedMotor left = new EV3LargeRegulatedMotor(MotorPort.D);
		final EV3LargeRegulatedMotor right = new EV3LargeRegulatedMotor(MotorPort.A);

		Button.waitForAnyPress();

		left.setSpeed(100);
		right.setSpeed(40);

		left.forward();
		right.forward();
		Delay.msDelay(5000);
	}

	@Override
	public void rotationStarted(RegulatedMotor motor, int tachoCount, boolean stalled, long timeStamp) {
		LCD.drawString("I am Rotating", 0, 0);

	}

	@Override
	public void rotationStopped(RegulatedMotor motor, int tachoCount, boolean stalled, long timeStamp) {
		LCD.drawString("Rotation Stopped", 0, 0);

	}

}
